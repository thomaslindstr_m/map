// ---------------------------------------------------------------------------
//  map
//  return items from an array or object based on a mapping function
// ---------------------------------------------------------------------------

var isUndefined = require('@amphibian/is-undefined');
var isArray = require('@amphibian/is-array');
var isObject = require('@amphibian/is-object');
var iterateUpArray = require('@amphibian/iterate-up-array');
var forOwn = require('@amphibian/for-own');

/**
 * Map
 * @param {array|object} input
 * @param {function} mapper
**/
function map(input, mapper) {
    var result = [];

    if (isArray(input)) {
        iterateUpArray(input, function (index, i, end) {
            var mapperResult = mapper(index, i, end, result);

            if (!isUndefined(mapperResult)) {
                result.push(mapperResult);
            }
        });
    } else if (isObject(input)) {
        forOwn(input, function (key, value, end) {
            var mapperResult = mapper(key, value, end, result);

            if (!isUndefined(mapperResult)) {
                result.push(mapperResult);
            }
        });
    }

    return result;
}

module.exports = map;
