// ---------------------------------------------------------------------------
//  test.js
// ---------------------------------------------------------------------------

var map = require('./index.js');
var assert = require('assert');

describe('map', function () {
    var array = ['zero', 'one', 'two', 'three', 'four'];
    var object = {
        zero: {hello: true},
        one: {hello: false},
        two: {hello: true},
        three: {hello: false},
        four: {hello: false}
    };

    it('should map arrays', function () {
        assert.deepEqual(
            map(array, (index) => index[0]),
            ['z', 'o', 't', 't', 'f']
        );
    });

    it('should discard undefined, but not 0', function () {
        assert.deepEqual(
            map(array, (index, i) => {
                if (i === 1) {
                    return;
                }

                return i;
            }),
            [0, 2, 3, 4]
        );
    });

    it('should map arrays with results', function () {
        assert.deepEqual(
            map(array, (index, i, end, results) => {
                if (results.indexOf(index[0]) === -1) {
                    return index[0];
                }
            }),
            ['z', 'o', 't', 'f']
        );
    });

    it('should map on array index value', function () {
        assert.deepEqual(
            map(array, (index) => index !== 'zero'),
            [false, true, true, true, true]
        );
    });

    it('should map on array index position', function () {
        assert.deepEqual(
            map(array, (index, i) => i < 3),
            [true, true, true, false, false]
        );
    });

    it('should map on array result length', function () {
        assert.deepEqual(
            map(array, (index, i, end, results) => results.length < 2),
            [true, true, false, false, false]
        );
    });

    it('should stop mapping array on end', function () {
        var matches = map(array, (index, i, end, matches) => {
            if (matches.length < 2) {
                return i;
            } else {
                end();
                return i;
            }
        });

        assert.deepEqual(matches, [0, 1, 2]);
    });

    it('should map on object key', function () {
        assert.deepEqual(
            map(object, (key) => key === 'zero'),
            [true, false, false, false, false]
        );
    });

    it('should map on object value', function () {
        assert.deepEqual(
            map(object, (key, value) => value.hello === true),
            [true, false, true, false, false]
        );
    });

    it('should map on object results value', function () {
        assert.deepEqual(
            map(object, (key, value, end, result) => !Boolean(result[1])),
            [true, true, false, false, false]
        );
    });

    it('should stop mapping object on end', function () {
        var match = map(object, (key, value, end) => {
            if (value.hello === true) {
                return true;
            } else {
                end();
                return false;
            }
        });

        assert.deepEqual(match, [true, false]);
    });
});
