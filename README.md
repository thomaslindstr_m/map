# map

[![build status](https://gitlab.com/thomaslindstr_m/map/badges/master/build.svg)](https://gitlab.com/thomaslindstr_m/map/commits/master)

return items from an array or object based on a mapping function

```
npm install @amphibian/map
```

```javascript
var map = require('@amphibian/map');
var array = ['zero', 'one', 'two', 'three', 'four'];

map(array, (index, i) => i); // > [0, 1, 2, 3, 4]
map(array, (index, i) => {
    if (i === 1) {
        return; // NOTE `undefined` is ignored
    }

    return i + 10;
}); // > [10, 12, 13, 14]

var object = {
    zero: {hello: true},
    one: {hello: false},
    two: {hello: true},
    three: {hello: false}
};

map(object, (key, value) => value); /* > [
    {hello: true}, {hello: false}, {hello: true}, {hello: false}
] */

// You can also stop the mapping when you're satisfied with the results
var results = map(array, (index, i, end, results) => {
    if (results.length < 2) {
        return index;
    } else {
        end();
        return index;
    }
});

console.log(results); // > ['zero', 'one', 'two']
```
